# TODO: __docs__


class MainHandler:
    def __init__(self, org_container, cond_container):
        self.org = org_container
        self.conditions = cond_container

    def get_rule(self, rule_name):
        return self.conditions.get(rule_name)

    def check_result(self, handle_type, result_string):
        if handle_type == 'result':
            return result_string
        elif handle_type == 'rule':
            return self.execute(rule_name=result_string)

    def execute(self, rule_name='rule_1'):
        condition_handler = self.get_if_handler(rule_name)
        if condition_handler.check_for_true():
            return self.handle('then', rule_name)
        if condition_handler.found:
            return self.handle('else', rule_name)
        return self.handle('not_found', rule_name)

    def get_if_handler(self, rule_name):
        rule = self.get_rule(rule_name)
        if_condition = rule.get('if')
        return IfConditionHandler(self.org, if_condition)

    def handle(self, param, rule_name):
        condition_handler = self.get_handler(param, rule_name)
        handle_type, result_string = condition_handler.get_result()
        return self.check_result(handle_type, result_string)

    def get_handler(self, handle_type, rule_name):
        rule = self.get_rule(rule_name)
        condition = rule.get(handle_type)
        return ConditionHandler(condition)


class IfConditionHandler:
    def __init__(self, org_container, cond_container):
        self.found = True
        self.org_container = org_container
        self.cond_container = cond_container
        self.condition = getattr(self, self.cond_container.get('cond'))

    def greater_then_on_equal_to(self, org_value):
        value = self.cond_container.get('value')
        if org_value >= value:
            return True

    def range(self, org_value):
        minimum = self.cond_container.get('min')
        maximum = self.cond_container.get('max')
        if org_value in range(minimum, maximum):
            return True

    def equal(self, org_value):
        value = self.cond_container.get('value')
        if org_value == value:
            return True

    def get_org_value(self):
        f = self.cond_container.get('field')
        if self.org_container.get(f) is None:
            self.found = False
        return self.org_container.get(f)

    def check_for_true(self):
        org_value = self.get_org_value()
        return self.condition(org_value)


class ConditionHandler:
    def __init__(self, cond_container):
        self.cond_container = cond_container
        self.type = self.cond_container.get('type')

    def rule(self):
        return 'rule', self.cond_container.get('name')

    def result(self):
        message = self.cond_container.get('message')
        if 'action' in self.cond_container.kwargs:
            action = self.cond_container.get('action')
            return 'result', f'result: {message}\naction: {action}'
        return 'result', f'result: {message}'

    def get_result(self):
        if hasattr(self, self.type):
            return getattr(self, self.type)()
        raise AttributeError(f'Unexpected condition type: "{self.type}"')
