from containers import Organization, Condition
from handlers import MainHandler
import glob

organizations = [Organization(org) for org in glob.glob('organization*') if org.endswith('.json')]
conditions = [Condition(org) for org in glob.glob('condition*') if org.endswith('.json')]

for org in organizations:
    for cond in conditions:
        handler = MainHandler(org, cond)
        result = handler.execute()
        print(result)
    print()

