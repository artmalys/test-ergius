import json
import logging


logger = logging.Logger(name='ContainerLogger')


def read_json(json_file):
    with open(json_file) as cond1:
        json_string = cond1.read()
        try:
            return json.loads(json_string)
        except json.JSONDecodeError:
            import codecs
            decoded_data = codecs.decode(json_string.encode(), 'utf-8-sig')
            return json.loads(decoded_data)


class Condition:
    def __init__(self, json_file=None, json_data=None, field=None):
        self.kwargs = {}
        self.field = field
        if json_file:
            self.field = json_file
            self.kwargs = read_json(json_file)
        elif json_data:
            self.kwargs = json_data

    def get(self, item):
        val = self.kwargs.get(item)
        if val is None:
            raise KeyError(f'Condition field not found: "{item}" in "{self.field}"')
        if isinstance(val, dict):
            val = Condition(json_data=val, field=item)
        return val


class Organization(Condition):
    def get(self, item):
        val = self.kwargs
        for i in item.split('.'):
            if isinstance(val, dict):
                val = val.get(i)
                if val is None:
                    logger.warning(f'Organization field not found: "{item}" in {self.field}')
            else:
                logger.warning(f'Organization field not found: "{item}" in {self.field}')
                return
        return val
